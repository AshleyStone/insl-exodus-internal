/**
 * @file exopastebin.cpp
 * @brief Helpers for posting to pastebin-like services.
 *
 * $LicenseInfo:firstyear=2012&license=viewerlgpl$
 * Copyright (C) 2012 Katharine Berry
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "linden_common.h"

#include "llbufferstream.h"
#include "llhttpclient.h"
#include "lluri.h"
#include "stringize.h"

#include "llclipboard.h"
#include "llui.h"

#include "exopastebinkeys.h"
#include "exopastebin.h"

class exoPastebinResponse : public LLHTTPClient::Responder
{
public:
	exoPastebinResponse(const exoPastebin::completion_function& callback);
	/*virtual*/ void result(const LLSD& content);
	/*virtual*/ void errorWithContent(U32 status, const std::string& reason, const LLSD& content);
	/*virtual*/ void error(U32 status, const std::string& reason);
private:
	exoPastebin::completion_function mCallback;
};

//static
void exoPastebin::generateLink(const std::string& content, const exoPastebin::completion_function& callback)
{
	LLSD params;
	params["apikey"] = EXO_PASTEBIN_KEY;
	params["type"] = "text";
	params["data"] = content;
	LLHTTPClient::post("http://p.exodusviewer.com/submit/", params, new exoPastebinResponse(callback));
}

//static
bool exoPastebin::hasPasteData()
{
	return LLClipboard::instance().isTextAvailable();
}

//static
void exoPastebin::doPaste(const completion_function& callback)
{
	if(hasPasteData()) {
		LLWString paste;
		LLClipboard::instance().pasteFromClipboard(paste, false);
		generateLink(wstring_to_utf8str(paste), callback);
		make_ui_sound("UISndExodusPastebinPaste");
	}
}

//static
void exoPastebin::copyLinkToClipboard(const LLWString& text, S32 start, S32 len)
{
	std::string content = wstring_to_utf8str(text.substr(start, len));
	generateLink(content, &exoPastebin::putGeneratedLinkOnClipboard);
}

// static
void exoPastebin::putGeneratedLinkOnClipboard(const LLWString& text)
{
	LLClipboard::instance().copyToClipboard(text, 0, text.length());
	make_ui_sound("UISndExodusPastebinCopy");
}

exoPastebinResponse::exoPastebinResponse(const exoPastebin::completion_function& callback) :
mCallback(callback)
{
}

void exoPastebinResponse::result(const LLSD& content)
{
	mCallback(utf8string_to_wstring(content["URL"]));
}

void exoPastebinResponse::errorWithContent(U32 status, const std::string& reason, const LLSD& content)
{
	if(status == 500 && content.has("FailureReason"))
	{
		mCallback(utf8string_to_wstring("[" + std::string(content["FailureReason"]) + "]"));
	}
	else
	{
		mCallback(utf8string_to_wstring(STRINGIZE("[error " << status << "]")));
	}
}

void exoPastebinResponse::error(U32 status, const std::string &reason)
{
	mCallback(utf8string_to_wstring(STRINGIZE("[error " << status << "]")));
}
