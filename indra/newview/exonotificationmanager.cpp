/**
 * @file exonotificationmanager.cpp
 * @brief Manages display of platform-specific toast systems.
 *
 * $LicenseInfo:firstyear=2012&license=viewerlgpl$
 * Copyright (C) 2012 Katharine Berry
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "llagentdata.h"
#include "llappviewer.h"
#include "lldir.h"
#include "llfile.h"
#include "llfocusmgr.h"
#include "llimview.h"
#include "llnotifications.h"
#include "llsd.h"
#include "llsdserialize.h"
#include "llstartup.h"
#include "llviewercontrol.h"
#include "llviewerwindow.h"
#include "llwindow.h"

#include "exonotificationmanager.h"
#include "exonotifier.h"

// Platform-specific includes
#ifdef LL_DARWIN
#include "exonotifiermacosx.h"
#endif

exoNotificationManager::exoNotificationManager() : LLEventTimer(EXO_NOTIFICATION_THROTTLE_CLEANUP_PERIOD)
{
	// Create a notifier appropriate to the platform.
#ifdef LL_DARWIN
	mNotifier = new exoNotifierMacOSX();
#else
	mNotifier = new exoNotifier();
	LL_INFOS("exoNotifications") << "Created generic exoNotifier." << LL_ENDL;
#endif
	
	// Don't do anything more if notifications aren't usable
	if(!mNotifier->isUsable())
	{
		LL_WARNS("exoNotifications") << "Notifications unusable; bailing out." << LL_ENDL;
		return;
	}
}

exoNotificationManager::~exoNotificationManager()
{
	delete mNotifier;
}

void exoNotificationManager::initialize()
{
	// Hook into LLNotifications...
	// We hook into all of them, even though (at the time of writing) nothing uses "alert", so more notifications can be added easily.
	mNotificationChannel = new LLNotificationChannel("ExodusNotifications", "Visible", boost::bind(&exoNotificationManager::filterOldNotifications, this, _1));
	LLNotifications::instance().getChannel("ExodusNotifications")->connectChanged(boost::bind(&exoNotificationManager::onLLNotification, this, _1));
	
	// Also hook into IM notifications.
	LLIMModel::instance().mNewMsgSignal.connect(boost::bind(&exoNotificationManager::onInstantMessage, this, _1));
	
	loadConfig();
}

void exoNotificationManager::loadConfig()
{
	std::string config_file = gDirUtilp->getExpandedFilename(LL_PATH_APP_SETTINGS, "exodus_notifications.xml");
	if(config_file == "")
	{
		LL_WARNS("exoNotifications") << "Couldn't find exodus_notifications.xml" << LL_ENDL;
		return;
	}
	LL_INFOS("exoNotifications") << "Loading notification config from " << config_file << LL_ENDL;
	llifstream configs(config_file);
	LLSD notificationLLSD;
	std::set<std::string> notificationTypes;
	notificationTypes.insert(EXO_NOTIFICATION_IM_MESSAGE_TYPE);
	if(configs.is_open())
	{
		LLSDSerialize::fromXML(notificationLLSD, configs);
		for(LLSD::map_iterator itr = notificationLLSD.beginMap(); itr != notificationLLSD.endMap(); ++itr)
		{
			exoNotification ntype;
			ntype.name = itr->second.get("NotificationName").asString();
			notificationTypes.insert(ntype.name);
			
			if(itr->second.has("Title"))
				ntype.title = itr->second.get("Title").asString();
			if(itr->second.has("Body"))
				ntype.body = itr->second.get("Body").asString();
			if(itr->second.has("UseDefaultTextForTitle"))
				ntype.useDefaultTextForTitle = itr->second.get("UseDefaultTextForTitle").asBoolean();
			else
				ntype.useDefaultTextForTitle = false;
			if(itr->second.has("UseDefaultTextForBody"))
				ntype.useDefaultTextForBody = itr->second.get("UseDefaultTextForBody").asBoolean();
			else
				ntype.useDefaultTextForBody = false;
			if(ntype.useDefaultTextForBody == false && ntype.useDefaultTextForTitle == false &&
			   ntype.body.empty() && ntype.title.empty())
			{
				ntype.useDefaultTextForBody = true;
			}
			mNotifications[itr->first] = ntype;
		}
		configs.close();
		
		mNotifier->registerApplication(LLAppViewer::instance()->getSecondLifeTitle(), notificationTypes);
	}
	else
	{
		LL_WARNS("exoNotifications") << "Couldn't open notification config file." << LL_ENDL;
	}
	
}

void exoNotificationManager::notify(const std::string& notification_title, const std::string& notification_message, const std::string& notification_type)
{
	if(!shouldNotify())
		return;
	
	if(!gSavedSettings.getBOOL("ExodusEnableNotifications"))
		return;
	
	if(mNotifier->needsThrottle())
	{
		U64 now = LLTimer::getTotalTime();
		if(mTitleTimers.find(notification_title) != mTitleTimers.end())
		{
			if(mTitleTimers[notification_title] > now - EXO_NOTIFICATION_THROTTLE_TIME)
			{
				LL_WARNS("exoNotifications") << "Discarded notification with title '" << notification_title << "' - spam." << LL_ENDL;
				mTitleTimers[notification_title] = now;
				return;
			}
		}
		mTitleTimers[notification_title] = now;
	}
	
	mNotifier->showNotification(notification_title, notification_message.substr(0, EXO_NOTIFICATION_MAX_BODY_LENGTH), notification_type);
}

BOOL exoNotificationManager::tick()
{
	mTitleTimers.clear();
	return false;
}

bool exoNotificationManager::onLLNotification(const LLSD& notice)
{
	if(notice["sigtype"].asString() != "add")
		return false;
	if(!shouldNotify())
		return false;
	
	LLNotificationPtr notification = LLNotifications::instance().find(notice["id"].asUUID());
	std::string name = notification->getName();
	LLSD substitutions = notification->getSubstitutions();
	if(LLStartUp::getStartupState() < STATE_STARTED)
	{
		LL_WARNS("exoNotifications") << "exoNotificationManager discarded a notification (" << name << ") - too early." << LL_ENDL;
		return false;
	}
	if(mNotifications.find(name) != mNotifications.end())
	{
		exoNotification* exo_notification = &mNotifications[name];
		std::string body;
		std::string title;
		
		if(exo_notification->useDefaultTextForTitle)
			title = notification->getMessage();
		else if(!exo_notification->title.empty())
			title = exo_notification->title;
		
		LLStringUtil::format(title, substitutions);
		
		if(exo_notification->useDefaultTextForBody)
			body = notification->getMessage();
		else if(!exo_notification->body.empty())
			body = exo_notification->body;
		
		LLStringUtil::format(body, substitutions);
		
		notify(title, body, exo_notification->name);
	}
	return false;
}

bool exoNotificationManager::filterOldNotifications(LLNotificationPtr pNotification)
{
	// *HACK: I don't see any better way to avoid getting old, persisted messages...
	return (pNotification->getDate().secondsSinceEpoch() >= LLDate::now().secondsSinceEpoch() - 10);
}

void exoNotificationManager::onInstantMessage(const LLSD& im)
{
	// Don't show messages from ourselves or the system.
	LLUUID from_id = im["from_id"];
	if(from_id == LLUUID::null || from_id == gAgentID)
		return;
	std::string message = im["message"];
	std::string prefix = message.substr(0, 4);
	if(prefix == "/me " || prefix == "/me'")
	{
		message = message.substr(3);
	}
	notify(im["from"], message, EXO_NOTIFICATION_IM_MESSAGE_TYPE);
}

void exoNotificationManager::onAppFocusGained()
{
	mNotifier->clearDelivered();
}

//static
bool exoNotificationManager::shouldNotify()
{
	// This magic stolen from llappviewer.cpp. LLViewerWindow::getActive lies.
	return ((!gViewerWindow->mWindow->getVisible() || !gFocusMgr.getAppHasFocus()) || gSavedSettings.getBOOL("ExodusNotifyWhenActive"));
}
