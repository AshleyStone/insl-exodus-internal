/** 
 * @file exobridge.cpp
 *
 * eAPI is a simple set of tools designed to provide content
 * creators with the opportunity to use optional viewer side functions
 * in Exodus for their products in Second Life.
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011 Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#include "llviewerprecompiledheaders.h"

#include "exobridge.h"

// Stuff
#include "llagent.h"
#include "llstartup.h"
#include "llviewercontrol.h"
#include "llviewermessage.h"
#include "llviewerobject.h"
#include "llviewerobjectlist.h"
#include "llviewerwindow.h"
#include "llviewerregion.h"

// Raycasting
#include "pipeline.h"
#include "raytrace.h"

// Windlight
#include "llwlparammanager.h"
#include "llenvmanager.h"
#include "lldaycyclemanager.h"

// Camera
#include "llfocusmgr.h"
#include "llvoavatarself.h"
#include "llagentcamera.h"

// Chat given from scripts is handled here:
BOOL eAPI::phraseChat(std::string message, LLViewerObject* chatter, LLUUID from_id)
{
	if (message.length() <3) return FALSE;

	if (GENESIS_CMD_PREFIX == message[0])
	{
		std::string command;

		size_t colon = message.find(':');
		bool extras = colon != std::string::npos;

		if (extras) command = message.substr(0, colon);
		else command = message;

		if (command == "#sit")
		{
			if (!gSavedSettings.getBOOL("ExodusGenesisAPI")) return TRUE;

			LLMessageSystem* msg = gMessageSystem;

			msg->newMessageFast(_PREHASH_AgentRequestSit);
			msg->nextBlockFast(_PREHASH_AgentData);
			msg->addUUIDFast(_PREHASH_AgentID, gAgent.getID());
			msg->addUUIDFast(_PREHASH_SessionID, gAgent.getSessionID());
			msg->nextBlockFast(_PREHASH_TargetObject);
			msg->addUUIDFast(_PREHASH_TargetID, extras ? LLUUID(message.substr(colon + 1)) : from_id);
			msg->addVector3Fast(_PREHASH_Offset, LLVector3::zero);

			gAgent.sendReliableMessage();

			return TRUE;
		}
		else if (command == "#getresolution")
		{
			if (!gSavedSettings.getBOOL("ExodusGenesisAPI") || !extras) return TRUE;

			S32 channel = -1;
			LLMessageSystem* msg = gMessageSystem;

			LLStringUtil::convertToS32(message.substr(colon + 1), channel);

			msg->newMessage("ScriptDialogReply");
			msg->nextBlock("AgentData");
			msg->addUUID("AgentID", gAgent.getID());
			msg->addUUID("SessionID", gAgent.getSessionID());
			msg->nextBlock("Data");
			msg->addUUID("ObjectID", from_id);
			msg->addS32("ChatChannel", channel);
			msg->addS32("ButtonIndex", 1);
			msg->addString("ButtonLabel", llformat("%d,%d", gViewerWindow->getWindowWidthRaw(), gViewerWindow->getWindowHeightRaw()));

			gAgent.sendReliableMessage();

			return TRUE;
		}
		else if (command == "#unsit")
		{
			gAgent.standUp();

			return TRUE;
		}
		else if (command == "#ao")
		{
			if (extras)
			{
				std::string substring = message.substr(colon + 1);
				LLStringUtil::toUpper(substring);

				gSavedPerAccountSettings.setBOOL("ExodusAOEnable",
					(substring == "1" || substring == "true" || substring == "on"));
			}
			else
			{
				gSavedPerAccountSettings.setBOOL("ExodusAOEnable",
					!gSavedPerAccountSettings.getBOOL("ExodusAOEnable"));
			}

			return TRUE;
		}
		else if (command == "#touch")
		{
			if (!gSavedSettings.getBOOL("ExodusGenesisAPI")) return TRUE;

			LLViewerObject* target = extras ? gObjectList.findObject(LLUUID(message.substr(colon + 1))) : chatter;

			if (target)
			{
				LLMessageSystem* msg = gMessageSystem;
				LLViewerRegion* region = chatter->getRegion();

				msg->newMessageFast(_PREHASH_ObjectGrab);
				msg->nextBlockFast( _PREHASH_AgentData);
				msg->addUUIDFast(_PREHASH_AgentID, gAgent.getID());
				msg->addUUIDFast(_PREHASH_SessionID, gAgent.getSessionID());
				msg->nextBlockFast( _PREHASH_ObjectData);
				msg->addU32Fast(_PREHASH_LocalID, target->mLocalID);
				msg->addVector3Fast(_PREHASH_GrabOffset, LLVector3::zero);
				msg->nextBlock("SurfaceInfo");
				msg->addVector3("UVCoord", LLVector3::zero);
				msg->addVector3("STCoord", LLVector3::zero);
				msg->addS32Fast(_PREHASH_FaceIndex, 0);
				msg->addVector3("Position", target->getPosition());
				msg->addVector3("Normal", LLVector3::zero);
				msg->addVector3("Binormal", LLVector3::zero);
				msg->sendReliable(region->getHost());

				msg->newMessageFast(_PREHASH_ObjectDeGrab);
				msg->nextBlockFast(_PREHASH_AgentData);
				msg->addUUIDFast(_PREHASH_AgentID, gAgent.getID());
				msg->addUUIDFast(_PREHASH_SessionID, gAgent.getSessionID());
				msg->nextBlockFast(_PREHASH_ObjectData);
				msg->addU32Fast(_PREHASH_LocalID, target->mLocalID);
				msg->nextBlock("SurfaceInfo");
				msg->addVector3("UVCoord", LLVector3::zero);
				msg->addVector3("STCoord", LLVector3::zero);
				msg->addS32Fast(_PREHASH_FaceIndex, 0);
				msg->addVector3("Position", target->getPosition());
				msg->addVector3("Normal", LLVector3::zero);
				msg->addVector3("Binormal", LLVector3::zero);
				msg->sendReliable(region->getHost());
			}

			return TRUE;
		}
		else if (command == "#rez")
		{
			if (!gSavedSettings.getBOOL("ExodusGenesisAPI") || !extras) return TRUE;

			LLSD args;

			U32 count = 0;
			std::string token;
			std::istringstream iss(message.substr(colon + 1));

			while (getline(iss, token, ','))
			{
				args[count] = token;
				count += 1;
			}

			std::string inv_name = args[0].asString();

			if (!inv_name.empty() && args.size() == 4)
			{
				F32 x, z;
				F32 y = args[2].asReal();

				std::string temp;

				if ((temp = args[1].asString()) != "" && temp[0] == '<')
				{
					x = atof(temp.substr(1).c_str());
				}
				else x = args[1].asReal();

				if ((temp = args[3].asString()) != "" && temp[temp.length() - 1] == '>')
				{
					z = atof(temp.substr(0, temp.length() - 1).c_str());
				}
				else z = args[3].asReal();

				if (chatter)
				{
					LLInventoryObject::object_list_t obj_inv;
					chatter->getInventoryContents(obj_inv);

					if (!obj_inv.empty())
					{
						LLVector3 target = LLVector3(llclamp(x, 0.f, 256.f), llclamp(y, 0.f, 256.f), llclamp(z, 0.f, 4096.f));
						LLInventoryObject::object_list_t::iterator obj_it;

						for (obj_it = obj_inv.begin(); obj_it != obj_inv.end(); ++obj_it)
						{
							LLInventoryItem* inventory = (LLInventoryItem*)((LLInventoryObject*)(*obj_it));

							if (inventory->getName() == inv_name)
							{
								LLPermissions perms = inventory->getPermissions();

								U32 group_mask = perms.getMaskGroup();
								U32 everyone_mask = perms.getMaskEveryone();
								U32 next_owner_mask = perms.getMaskNextOwner();

								LLMessageSystem* msg = gMessageSystem;

								msg->newMessageFast(_PREHASH_RezObject);
								msg->nextBlockFast(_PREHASH_AgentData);
								msg->addUUIDFast(_PREHASH_AgentID,  gAgent.getID());
								msg->addUUIDFast(_PREHASH_SessionID,  gAgent.getSessionID());
								msg->addUUIDFast(_PREHASH_GroupID, gAgent.getGroupID());
								msg->nextBlock("RezData");
								msg->addUUIDFast(_PREHASH_FromTaskID, from_id);
								msg->addU8Fast(_PREHASH_BypassRaycast, (U8)TRUE);
								msg->addVector3Fast(_PREHASH_RayStart, target);
								msg->addVector3Fast(_PREHASH_RayEnd, target);
								msg->addUUIDFast(_PREHASH_RayTargetID, LLUUID::null);
								msg->addBOOLFast(_PREHASH_RayEndIsIntersection, FALSE);
								msg->addBOOLFast(_PREHASH_RezSelected, FALSE);
								msg->addBOOLFast(_PREHASH_RemoveItem, FALSE);
								msg->addU32Fast(_PREHASH_ItemFlags, inventory->getFlags());
								msg->addU32Fast(_PREHASH_GroupMask, group_mask);
								msg->addU32Fast(_PREHASH_EveryoneMask, everyone_mask);
								msg->addU32Fast(_PREHASH_NextOwnerMask, next_owner_mask);
								msg->nextBlockFast(_PREHASH_InventoryData);

								inventory->packMessage(msg);

								gAgent.sendReliableMessage();
							}
						}
					}
				}
			}

			return TRUE;
		}
		else if (command == "#freeze") // for selecting then deselecting objects with a velocity.
		{
			if (gSavedSettings.getBOOL("ExodusGenesisAPI")) return TRUE;

			LLMessageSystem* msg = gMessageSystem;
			LLViewerObject* target = chatter;

			if (extras)
			{
				target = gObjectList.findObject(LLUUID(message.substr(colon + 1)));
			}

			if (target)
			{
				LLViewerRegion* region = chatter->getRegion();

				msg->newMessageFast(_PREHASH_ObjectSelect);
				msg->nextBlockFast(_PREHASH_AgentData);
				msg->addUUIDFast(_PREHASH_AgentID, gAgent.getID());
				msg->addUUIDFast(_PREHASH_SessionID, gAgent.getSessionID());
				msg->nextBlockFast(_PREHASH_ObjectData);
				msg->addU32Fast(_PREHASH_ObjectLocalID, chatter->getLocalID());
				msg->sendReliable(region->getHost());

				msg->newMessageFast(_PREHASH_ObjectDeselect);
				msg->nextBlockFast(_PREHASH_AgentData);
				msg->addUUIDFast(_PREHASH_AgentID, gAgent.getID());
				msg->addUUIDFast(_PREHASH_SessionID, gAgent.getSessionID());
				msg->nextBlockFast(_PREHASH_ObjectData);
				msg->addU32Fast(_PREHASH_ObjectLocalID, chatter->getLocalID());
				msg->sendReliable(region->getHost());
			}

			return TRUE;
		}
		else if (command == "#load")
		{
			if (gSavedSettings.getBOOL("ExodusGenesisAPI"))
			{
				if (LLStartUp::getStartupState() >= STATE_STARTED && chatter)
				{
					chatter->requestInventory();
				}
			}

			return TRUE;
		}
		else if (command == "#rc")
		{
			if (!gSavedSettings.getBOOL("ExodusGenesisAPI") || !extras) return TRUE;

			LLSD args;
			U32 count = 0;

			std::string token;
			std::istringstream iss(message.substr(colon + 1));

			while (getline(iss, token, ',')){ args[count] = token; ++count; }

			if (args[0].asString() == "once" && args.size() > 1)
			{
				std::string label = (args.size() > 3) ? args[3].asString() : "once";

				S32 x = gViewerWindow->getCurrentMouseX();
				S32 y = gViewerWindow->getCurrentMouseY();

				F32 depth = ((args.size() == 3) ? args[2].asReal() : 256.f);

				LLVector3 mouse_direction_global = gViewerWindow->mouseDirectionGlobal(x, y);
				LLVector3 mouse_point_global = LLViewerCamera::getInstance()->getOrigin();
				LLVector3 n = LLViewerCamera::getInstance()->getAtAxis();
				LLVector3 p = mouse_point_global + n * LLViewerCamera::getInstance()->getNear();
				LLVector3 target; line_plane(mouse_point_global, mouse_direction_global, p, n, target);
				LLVector3 source = target + mouse_direction_global * depth;

				S32 castFace = -1;

				LLVector4a castIntersection = LLVector4a(0.f);
				LLVector2 castTextureCoord;
				LLVector4a castNormal;
				LLVector4a castBinormal;

				if (from_id.isNull()) from_id = gAgent.getID();

				LLViewerObject* castObject = NULL;
                
                LLVector4a target4a, source4a;
                target4a.load3(target.mV);
                source4a.load3(source.mV);
                
				castObject = gPipeline.lineSegmentIntersectInWorld(target4a, source4a, FALSE, &castFace, &castIntersection, &castTextureCoord, &castNormal, &castBinormal);
                
                LLVector3 castIntersection3(castIntersection[0], castIntersection[1], castIntersection[2]);
                LLVector3 castNormal3(castNormal[0], castNormal[1], castNormal[2]);
                LLVector3 castBinormal3(castBinormal[0], castBinormal[1], castBinormal[2]);
                
				LLVector3 translate = castIntersection3;
				LLMessageSystem* msg = gMessageSystem;

				msg->newMessage("ScriptDialogReply");
				msg->nextBlock("AgentData");
				msg->addUUID("AgentID", gAgent.getID());
				msg->addUUID("SessionID", gAgent.getSessionID());
				msg->nextBlock("Data");
				msg->addUUID("ObjectID", from_id);
				msg->addS32("ChatChannel", args[1].asInteger());
				msg->addS32("ButtonIndex", 1);

				if (castObject)
				{
					LLCoordFrame orient;
					orient.lookDir(castNormal3, castBinormal3);
					LLQuaternion rotation = orient.getQuaternion();
					std::string key = castObject->getID().asString();

					if (from_id.isNull()) castObject->getID();

					msg->addString("ButtonLabel", llformat(
						"#rc,<%f,%f,%f>,<%f,%f,%f,%f>,",
						translate.mV[0], translate.mV[1],translate.mV[2],
						rotation.mQ[0], rotation.mQ[1], rotation.mQ[2], rotation.mQ[3]) +
						key + "," + label + llformat(",%d", gFrameCount)
					);
				}
				else
				{
					if (!translate.isNull())
					{
						LLCoordFrame orient;
						orient.lookDir(castNormal3, castBinormal3);
						LLQuaternion rotation = orient.getQuaternion();

						msg->addString("ButtonLabel", llformat(
							"#rc,<%f,%f,%f>,<%f,%f,%f,%f>,",
							translate.mV[0], translate.mV[1],translate.mV[2],
							rotation.mQ[0], rotation.mQ[1], rotation.mQ[2], rotation.mQ[3]) +
							"null," + label + llformat(",%d", gFrameCount)
						);
					}
					else
					{
						msg->addString("ButtonLabel", llformat("#rc,null,null,null,%s,%d", label.c_str(), gFrameCount));
					}
				}

				gAgent.sendReliableMessage();
			}

			return TRUE;
		}
		else if (command == "#wl")
		{
			if (!gSavedSettings.getBOOL("ExodusGenesisAPI") || !extras) return TRUE;
			
			if(message == "#wl:save")
			{
				// Apparently this isn't needed anymore?
				//LLEnvManagerNew::instance().saveUserPrefs();
			}
			else if (message == "#wl:load")
			{
				LLEnvManagerNew::instance().usePrefs();
			}
			else if (message == "#wl:clear")
			{
				LLEnvManagerNew::instance().usePrefs();
			}

			return TRUE;
		}
	}

	return FALSE;
}
