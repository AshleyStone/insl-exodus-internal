/** 
 * @file exofloaterinventory.h
 * @brief exoFloaterInventory class definitions
 *
 * $LicenseInfo:firstyear=2011&license=viewerlgpl$
 * Copyright (C) 2011, Ayamo Nozaki
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXO_FLOATER_INVENTORY
#define EXO_FLOATER_INVENTORY

#include "llfloater.h"

class LLInventoryPanel;
class LLScrollListCtrl;
class LLFolderViewItem;

class exoFloaterInventory :
	public LLFloater
{
	friend class LLFloaterReg;

private:
	exoFloaterInventory(const LLSD& key);
	~exoFloaterInventory();

	BOOL postBuild();

	LLInventoryPanel* folders;
	LLScrollListCtrl* contents;

public:
	void onSelectionChange(const std::deque<LLFolderViewItem*>& items, BOOL user_action);
};

#endif // EXO_FLOATER_INVENTORY
