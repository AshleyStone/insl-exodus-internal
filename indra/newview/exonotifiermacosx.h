/**
 * @file exonotifiermacosx.h
 * @brief Displays notifications via Notification Center
 *
 * $LicenseInfo:firstyear=2012&license=viewerlgpl$
 * Copyright (C) 2012 Katharine Berry
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation;
 * version 2.1 of the License only.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * $/LicenseInfo$
 */

#ifndef EXONOTIFIERMACOSX_H
#define EXONOTIFIERMACOSX_H

#include <string>
#include <set>
#include "exonotifier.h"

class exoNotifierMacOSX : public exoNotifier
{
public:
	exoNotifierMacOSX();
	/* virtual */ void showNotification(const std::string& notification_title, const std::string& notification_message, const std::string& notification_type);
	/* virtual */ bool isUsable();
	/* virtual */ bool needsThrottle() { return false; }
	/* virutal */ void clearDelivered();

private:
	static void notificationActivated();
};


#endif // EXONOTIFIERMACOSX_H
